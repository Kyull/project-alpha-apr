# Generated by Django 4.1.4 on 2022-12-06 22:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="task",
            name="due_date",
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name="task",
            name="is_completed",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="task",
            name="name",
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AddField(
            model_name="task",
            name="start_date",
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
